<?php


namespace DefStudio\Translator;


use DefStudio\Translator\Commands\CheckCommand;
use DefStudio\Translator\Contracts\TranslationService;
use DefStudio\Translator\Services\FileTranslationService;
use Illuminate\Support\ServiceProvider;

class TranslatorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {

            $this->commands([
                CheckCommand::class,
            ]);

        }

        $this->app->singleton(TranslationService::class, FileTranslationService::class);
    }
}
