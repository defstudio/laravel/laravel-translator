<?php


namespace DefStudio\Translator\Contracts;


interface TranslationService
{
    public function available_languages(): array;

    public function main_language(): string;

    public function get_translations_containers(string $language): array;

    public function get_translations(string $language, string $translation_file): array;

    public function has_translation(string $language, string $key): bool;
}
