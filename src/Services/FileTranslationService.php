<?php


namespace DefStudio\Translator\Services;


use DefStudio\Translator\Contracts\TranslationService;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class FileTranslationService implements TranslationService
{
    private string $main_language;
    private array $available_languages;

    public function available_languages(): array
    {
        return $this->available_languages ?? array_map('basename', glob(resource_path('lang/*'), GLOB_ONLYDIR));
    }

    public function main_language(): string
    {
        return $this->main_language ?? config('app.locale', config('app.fallback_locale'));
    }

    public function get_translations_containers(string $language): array
    {
        return array_map(function ($translation_file) {
            return Str::replaceLast(".php", "", $translation_file);
        }, array_diff(scandir(resource_path("lang/$language")), ['.', '..']));
    }


    public function get_translations(string $language, string $translation_file): array
    {
        return Lang::get($translation_file, [], $language, false) ?? [];
    }

    public function has_translation(string $language, string $key): bool
    {
        return Lang::hasForLocale($key, $language);
    }
}
