<?php


namespace DefStudio\Translator\Commands;


use DefStudio\Translator\Contracts\TranslationService;
use Illuminate\Console\Command;

class CheckCommand extends Command
{
    protected $signature = 'translator:check';

    protected $description = 'Check if translations are synced in all available languages';

    public function handle(TranslationService $translation_service)
    {

        $this->info("Checking available translations");

        $main_language = $translation_service->main_language();

        $this->info("Main language: $main_language");

        $available_languages = $translation_service->available_languages();

        $missing_translations_headers = [
            $main_language
        ];

        foreach ($available_languages as $language) {
            if ($language == $translation_service->main_language()) continue;

            $missing_translations_headers[] = $language;
        }

        $missing_translations = [];

        foreach ($available_languages as $language) {
            if ($language == $translation_service->main_language()) continue;

            $this->info("Checking against $language");

            foreach ($translation_service->get_translations_containers($main_language) as $translation_container) {
                foreach ($translation_service->get_translations($main_language, $translation_container) as $key => $value) {
                    if (!$translation_service->has_translation($language, $key)) {
                        $missing_translations["$translation_container.$key"][] = $language;
                    }
                }
            }

        }


        $missing_translation_table = [];

        foreach ($missing_translations as $key => $incomplete_languages) {
            $row = [];

            foreach ($missing_translations_headers as $header_language) {
                if ($header_language == $main_language) {
                    $row[] = $key;
                } else {
                    if (in_array($header_language, $incomplete_languages)) {
                        $row[] = 'x';
                    } else {
                        $row[] = '';
                    }
                }
            }

            $missing_translation_table[] = $row;
        }


        $this->table($missing_translations_headers, $missing_translation_table);

        return 0;
    }


}
